

# sass
Directories based on DevTips opinion from Travis Neilson`
@see https://www.youtube.com/watch?v=F3u7whqIxrc (2:37)
@see https://www.youtube.com/watch?v=GTBaQ2DcGUk (11:48)
- 1-tools --> libraries like bootstrap
- 2-base --> app specific modications like body, h1, h3, tables etc
- 3-sections --> eg books, icons, coins
- 4-modules --> eg. contact form