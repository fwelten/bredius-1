#!/usr/bin/env bash

OUT="../tmp"
LARGE="../bredius/client/dist/images/icons/large"
SMALL="../bredius/client/dist/images/icons/small"
cd pdfs
for i in *.pdf; do

#       pat=3909_047
#       pat=390
        if [[ $i =~ $pat ]]; then

                mkdir -p $OUT
                rm -fr $OUT/*

                PDF="${i/\.pdf/}"

                IMAGES=`pdfimages -list $i|wc -l`
                FILENAME="${i/\.pdf/}"

                echo ">>>>> Extracting images from pdf: $i"
                pdfimages -list $i > out.log
                pdfimages -png $i $OUT/$PDF

                FILES=`ls -l $OUT|awk {'print $9'}`

                CMD="convert"
                for f in $FILES; do
                        CMD+=" $OUT/$f"
                done
                CMD+=" -append $LARGE/$FILENAME.png"

                echo ">>>>> Stitching images (if any): $CMD"
                `$CMD`

                CMD="convert -resize 300x300  $LARGE/$FILENAME.png $SMALL/$FILENAME.png"
                echo ">>>>> Creating thumbnail: $CMD"
                `$CMD`

                echo
                echo

        fi

done