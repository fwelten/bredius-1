/**
 * Created by theotheu on 25-07-16.
 */
var fs = require('fs'),
    mongoose = require('mongoose'),
    env, config, database,
    http = require('http'),
    Stream = require('stream').Transform,
    request = require('request');


/**
 * Load configuration
 */
env = process.env.NODE_ENV || 'development'; // You can use the environment-variable 'NODE_ENV' to determine the environment being used. Fall back to 'development' when this is unset.
config = require('../server/config/config.js')[env]; // Load configuration file;
database = config.db;

mongoose.connect(database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
    var schemaName = mongoose.Schema({
        "database": {
            "text": {"type": String, "required": true},
            "name": {"type": String, "required": true},
            "path": {"type": String, "required": true}
        },
        "sourceApp": {
            "text": {"type": String, "required": true},
            "name": {"type": String, 'required': true},
            "version": {"type": String, "required": true}
        },
        "recNumber": {"type": String, "required": true},
        "refType": {
            "text": {"type": String},
            "name": {"type": String}
        },

        "contributors": {
            "authors": [],
            "secondaryAuthors": [],
            "tertiaryAuthors": []
        },
        "titles": {
            "title": {"type": String},
            "tertiaryTitle": {"type": String}
        },
        "pages": {
            "name": {type: String},
            "start": {type: String},
            "end": {type: String},
        },
        "reprintStatus": {
            "status": {"type": String}
        },
        "numVols": [],
        "keywords": [],
        "dates": [],
        "urls": {},
        "pubLocation": {"type": String},
        "notes": {"type": String},
        "custom1": {"type": String},
        "custom2": {"type": String},
        "custom3": {"type": String},
        "custom4": {"type": String},
        "misc": {"type": String},
        "misc1": {"type": String},
        "misc4": {"type": String},
        "creationDate": {type: Date, default: Date.now},
        "modificationDate": {type: Date, default: Date.now},
        "createdBy": {type: String, default: "admin"},
        "modifiedBy": {type: String, default: "admin"}
    });

    var IconModel = mongoose.model('Icon', schemaName);
    var Icon = mongoose.model('Icon'), url, filename, urls = [];

    Icon.find({}, {_id: 0, urls: 1})
        .sort({recNumber: 1})
        .exec(function (err, doc) {
            for (var i = 0; i < doc.length; i++) {
                url = null;
                if (doc[i]['urls']['pdf-urls']) {
                    url = doc[i]['urls']['pdf-urls'][0];
                }
                if (doc[i]['urls']['image-urls']) {
                    url = doc[i]['urls']['image-urls'][0];
                }
                if (doc[i]['urls']['related-urls']) {
                    url = doc[i]['urls']['related-urls'][0];
                }


                filename = url.split('/');
                filename = './pdfs/' + filename[filename.length - 1];

                urls.push({url: url, filename: filename});

            }
            processUrls(urls);

        });

});


function processUrls(urls) {

    if (urls.length === 0) {
        return;
    } else {

        var item, url, filename;
        item = urls.pop();
        url = item.url;
        filename = item.filename;

        // stats=fs.lstatSync(filename);


        fs.access(filename, fs.F_OK, function (err) {
            if (err === null) {
                console.log('DONE', url, filename);
                processUrls(urls);
            } else {
                console.log('RETRIEVING', urls.length, url, filename);
                request.head(url, function (err, res, body) {
                    if (res) {
                        request(url).pipe(fs.createWriteStream(filename)).on('close', function () {
                            processUrls(urls);
                        });
                    }
                });
            }
        });


    }
}