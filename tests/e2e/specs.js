// Load configuration
var env = process.env.NODE_ENV || 'development',
config = require('../../server/config/config.js')[env],
localConfig = require('./../config-test.json');

// The color for the headers is different because I'm using Bootstrap.
var HEADER_COLOR = 'rgba(51, 51, 51, 1)';

describe('Book test homepage', function () {

    beforeEach(function () {
        browser.get('http://' + localConfig.host + ':' + config.port);
    });

    it('should get the titles', function () {

        expect(browser.getTitle()).toBe('Book demo');
        expect(element(by.tagName('h1')).getText()).toBe('Book demo');
        expect(element(by.tagName('h2')).getText()).toBe('Books');

        // Get CSS value
        element(by.tagName('h1')).getCssValue('color')
        .then(function (v) {
            expect(v).toBe(HEADER_COLOR);
        });

    });

    it('should count the number of books', function () {

        var books = element.all(by.repeater('book in books'));

        expect(books.count()).toBe(10);

    });

    it('should get the first book', function () {

        var books = element.all(by.repeater('book in books'));

        expect(books.get(0).getText()).toEqual('DOCTOR SLEEP by Stephen King');

    });

    it('should get the last book', function () {

        var books = element.all(by.repeater('book in books'));

        expect(books.last().getText()).toEqual('SYCAMORE ROW by John Grisham');

    });

    it('should filter the books and return 1 book', function () {

        element(by.model('query')).sendKeys('tar');

        var books = element.all(by.repeater('book in books'));

        expect(books.count()).toBe(1);
        expect(books.get(0).getText()).toEqual('THE GOLDFINCH by Donna Tartt');

    });
});

describe('CRUD on book', function () {

    var _id;

    beforeEach(function () {
        browser.get('http://' + localConfig.host + ':' + config.port + '/#/books/new');
    });

    it('should get the titles', function () {

        browser.get('http://' + localConfig.host + ':' + config.port + '/#/books/new');

        expect(browser.getTitle()).toBe('Book demo');
        expect(element(by.tagName('h1')).getText()).toBe('Book demo');
        expect(element(by.tagName('h2')).getText()).toBe('New book');

        // Get CSS value
        element(by.tagName('h1')).getCssValue('color')
        .then(function (v) {
            expect(v).toBe(HEADER_COLOR);
        });
    });

    /**
    * @see https://docs.angularjs.org/api/ng/directive/form
    */
    it('should display an empty form', function () {

        browser.get('http://' + localConfig.host + ':' + config.port + '/#/books/new');

        expect(element(by.model('books.doc._id')).getText()).toBe('');
        expect(element(by.model('books.doc.title')).getText()).toBe('');
        expect(element(by.model('books.doc.author')).getText()).toBe('');
        expect(element(by.model('books.doc.description')).getText()).toBe('');

    });

    it('should create a book', function () {

        /**
        * First we create the new book
        */
        browser.get('http://' + localConfig.host + ':' + config.port + '/#/books/new');

        element(by.model('books.doc.title')).sendKeys('ALL THE LIGHT WE CANNOT SEE');
        element(by.model('books.doc.author')).sendKeys('Anthony Doerr');
        element(by.model('books.doc.description')).sendKeys('The lives of a blind French girl and a gadget-obsessed German boy before and during World War II.');

        element(by.id('save')).click();
        browser.switchTo().alert().accept();
    });

    it('should query the new created book', function () {

        browser.get('http://' + localConfig.host + ':' + config.port);

        element(by.model('query')).sendKeys('Anthony Doerr');

        var books = element.all(by.repeater('book in books'));

        expect(books.count()).toBe(1);
        expect(books.get(0).getText()).toEqual('ALL THE LIGHT WE CANNOT SEE by Anthony Doerr');

    });

    it('should update the new created book', function () {

        browser.get('http://' + localConfig.host + ':' + config.port);

        // Find the book
        element(by.model('query')).sendKeys('All');

        expect(element.all(by.repeater('book in books')).first().getText()).toBe('ALL THE LIGHT WE CANNOT SEE by Anthony Doerr');

        // Click on list item (note the nested selector)
        element.all(by.repeater('book in books')).first().click();

        // Retrieve id for later retrieval
        // Issue with retrieving value from input field, @see https://github.com/angular/protractor/issues/140
        element(by.model('books.doc._id')).getAttribute('value')
        .then(function (v) {
            _id = v;

            // Set new values
            element(by.model('books.doc.title')).clear();
            element(by.model('books.doc.title')).sendKeys('The Dreamer of the Snake');

            element(by.model('books.doc.author')).clear();
            element(by.model('books.doc.author')).sendKeys('Ruan Mashander');

            element(by.model('books.doc.description')).clear();
            element(by.model('books.doc.description')).sendKeys('Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');

            // Save new values
            element(by.id('save')).click();
            browser.switchTo().alert().accept();

            // Verify new values
            browser.get('http://' + localConfig.host + ':' + config.port);

            // Find the book
            element(by.model('query')).sendKeys(_id);

            expect(element.all(by.repeater('book in books')).first().getText()).toBe('The Dreamer of the Snake by Ruan Mashander');
        });

    });

    it('should delete the new created book', function () {

        browser.get('http://' + localConfig.host + ':' + config.port);

        // Find the book
        element(by.model('query')).sendKeys(_id);

        expect(element.all(by.repeater('book in books')).first().getText()).toBe('The Dreamer of the Snake by Ruan Mashander');

        // Click on list item (note the nested selector)
        element.all(by.repeater('book in books')).first().click();

        // Delete book
        element(by.id('delete')).click();
        browser.switchTo().alert().accept();

        // Verify that the number of books is 10
        browser.get('http://' + localConfig.host + ':' + config.port);

        var books = element.all(by.repeater('book in books'));

        expect(books.count()).toBe(10);
    });
});
