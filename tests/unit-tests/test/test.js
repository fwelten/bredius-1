var env = process.env.NODE_ENV || 'test',
    config = require('../../../server/config/config.js')[env],
    localConfig = require('./../../config-test.json');

var should = require('should'),
    supertest = require('supertest');

describe('API Routing for CRUD operations on books', function () {
    var request = supertest(localConfig.host + ":" + config.port + "/" + localConfig.api_path),
        tmpBookId = null,
        tmpBookResponse = null;

    before(function (done) {
        done();
    });

    describe('RETRIEVE all books', function () {
        it('Should GET /books', function (done) {
            request
                .get('/books')
                .expect(200)
                .expect('Content-Type', /application.json/)
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');

                    res.statusCode
                        .should.be.exactly(200);

                    // Store body of response for later reference
                    tmpBookResponse = parsed;
                    done();
                });
        });
    });

    describe('CREATE book', function () {
        it('Should POST /books', function (done) {
            request
                .post('/books')
                .send({
                    "title": "Great book!" + Date.now(),
                    "author": "John Doe",
                    "description": "Lorem ipsum dolor sit amet"
                })
                .expect(200) // Check status code with 'supertest'
                .expect('Content-Type', /application.json/) // Check content-type with 'supertest'
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('create');

                    parsed
                        .should.have.property('err').be.exactly(null);

                    // HTTP stuff has been tested with Supertest already, but check it again using 'should'
                    res.statusCode
                        .should.be.exactly(200);
                    res.type
                        .should.be.exactly('application/json');
                    res.charset
                        .should.be.exactly('utf-8');

                    // Store ID of created book for deletion later
                    tmpBookId = parsed.doc._id;
                    done();
                });
        });
    });

    describe('RETRIEVE 1 books', function () {
        it('Should GET /books/{id}', function (done) {
            request
                .get('/books/' + tmpBookId)
                .expect(200)
                .expect('Content-Type', /application.json/)
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('detail');

                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });

    describe('UPDATE 1 book', function () {
        it('Should PUT /books/{id}', function (done) {
            request
                .put('/books/' + tmpBookId)
                .send({
                    "doc": {
                        "title": "Good book " + Date.now(),
                        "author": "Better book",
                        "description": "Book is updated."
                    }
                })
                .expect(200)
                .expect('Content-Type', /application.json/)
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('update');

                    parsed
                        .should.have.property('err').be.exactly(null);

                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });

    describe('DELETE 1 books', function () {
        it('Should DELETE /books/{id}', function (done) {
            request
                .del('/books/' + tmpBookId)
                .expect(200)
                .expect('Content-Type', /application.json/)
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('delete');

                    parsed
                        .should.have.property('doc')
                        .and.have.property('n').be.exactly(1);

                    parsed
                        .should.have.property('doc')
                        .and.have.property('ok').be.exactly(1);

                    parsed
                        .should.have.property('err').be.exactly(null);

                    res.statusCode.should.be.exactly(200);

                    done();
                });
        });
    });


    describe('RETRIEVE all books to verify that the original collection is restored', function () {
        it('Should GET /books', function (done) {
            request
                .get('/books')
                .expect(200)
                .expect('Content-Type', /application.json/)
                .end(function (err, res) {
                    if(err) throw err;

                    // Parse body for testing
                    var parsed = JSON.parse(res.text);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');

                    res.statusCode
                        .should.be.exactly(200);

                    parsed
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly(tmpBookResponse.meta.action);
                    parsed
                        .should.have.property('meta')
                        .and.have.property('filename').be.exactly(tmpBookResponse.meta.filename);
                    parsed
                        .should.have.property('doc')
                        .and.have.property('length').be.exactly(tmpBookResponse.doc.length); // The number of books should not have changed...
                    parsed
                        .should.have.property('err').be.exactly(null);

                    done();
                });
        });
    });
});
