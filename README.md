Description
===========
This demo is MEAN with books.

Video available at http://goo.gl/KswfXf

Presentation available at http://goo.gl/idIiq3

**Your assignment: Fix the TODO's**

__Note (by Bas Dalenoord):__
_I'll describe progress in the file CHANGELOG.md to keep it nicely separated and structured. Answers to questions can be found in the file ANSWERS.md._

Setup
=====
Installation for development
----------------------------

Create a directory `workspaces` in your home directory.
```
mkdir ~/workspaces
```

Clone the repository with
```
git clone https://github.com/theotheu/books.git ~/workspaces/books
```

Go to the working directory
```
cd ~/workspaces/brediuslibrary
```

See the README in ~/workspaces/brediuslibrary/data how to import the initial seed


Configuration
----------
Copy ```config.js.default``` to ```config.js```.
```sh
cp ~/workspaces/brediuslibrary/server/config/config.js.default ~/workspaces/brediuslibrary/server/config/config.js
```

Change the database, port and emailaddress.

Example
```javascript
module.exports = {
    development: {
        db: 'mongodb://localhost/brediuslibrary-dev',
        port: 3000,
        debug: true
    }
    , test: {
        db: 'mongodb://localhost/brediuslibrary-tst',
        port: 3000,
        debug: false
    }
    , production: {
    }
}
```

Install node modules
----------
The archive is without the node modules.

Install with
```sh
cd ~/workspaces/brediuslibrary/server
npm install
```

Export variable to set environment
----------------------------------
Node.js starts default with the development environment.

To set explicitly the environment, use an environment variable. With this, you do not have to change a single line of code for switching environments.

Valid values are defined in the config/config.js file (development, test, production).

```export NODE_ENV=development```


supervisor
----------
Make sure you have `nodemon` installed - with the global option

```sh
sudo npm install -g nodemon
```

Use it with
```
nodemon
```

Client-side dependencies
------------------------
For managing the client dependencies I use [bower](http://bower.io/). There are 2 ways of handling dependencies with bower: checking them into the repostory and not checking them in to the repostory. General advice states the following:

>“If you aren’t authoring a package that is intended to be consumed by others (e.g., you’re building a web app), you should always check installed packages into source control.”

As per that advice, as we are developing a web app, I chose to check in the dependencies.

__Update__
If in the future you might want to update the dependencies, you can do so using the following steps:

1. Make sure you have `bower` installed with the global option
```
sudo npm install -g bower
```

2. To update the client-side dependencies, issue the following commands (from the root of the repository):
```
cd client/app
bower update
```

**Development**
The files to edit are in `~/workspaces/bredius/client/app`.

- Ruby is required for sass
  - Execute `sudo apt-get install ruby-sass` to have ruby available for Ubuntu

After you have changes files in that directory, run in the `~/workspaces/bredius/client/app` directory the command
```
grunt
```
The files are concatenated, minified and uglified in the `~/workspaces/bredius/client/dist` directory. This is the document root for the http server.


Tests
----------
See the README in `~/workspaces/brediuslibrary/tests` how to perform tests


Instructions to prepare a deployment
===================================

- Verify that you have a explanatory README.md
  - Installation instructions
  - Configuration instructions
  - Import data instructions
  - Run instructions
  - Test instructions
- Export data
  - Make sure that the output directory exist ```mkdir ~/workspaces/brediuslibrary/data```
  - Make an export of your data with mongodump ```mongodump -d brediuslibrary -o ~/workspaces/brediuslibrary/data```
  - Create in ~/workspaces/brediuslibrary/data a README.md with import instructions.
  - Import instructions
- Tests
  - static-analyzer with 0 errors
  - Unit tests with 0 errors
  - End-to-end tests with 0 errors
- Push the repository


Issues
======
When using dropbox together with git and you receive a message like `Git fatal: Reference has invalid format: 'refs/heads/master`, then run the next two commands (Dutch version).
```
find . -type f -name "* met conflict van *" -exec rm -f {} \;
awk '!/conflict/' .git/packed-refs > temp && mv temp .git/packed-refs
```